''' Challenge: Invert Keys and Values [InvertKeyValue]

Published by bangyen

Summary:
    Write a function that inverts the keys and values of a dictionary.

Examples:
    invert({ "z": "q", "w": "f" })
    ➞ { "q": "z", "f": "w" }

    invert({ "a": 1, "b": 2, "c": 3 })
    ➞ { 1: "a", 2: "b", 3: "c" }

    invert({ "zebra": "koala", "horse": "camel" })
    ➞ { "koala": "zebra", "camel": "horse" }

'''

def invert(dct):
    return {k : v for k, v in zip(dct.values(), dct.keys())}

if __name__ == "__main__":
    tests = [
        {"a": 1, "b": 2, "c": 3}, # {1: "a", 2: "b", 3: "c"}
        {"z": "q", "w": "f"}, # {"q": "z", "f": "w"}
        {"zebra": "koala", "horse": "camel"}, # {"koala": "zebra", "camel": "horse"}
    ]

    for t in tests:
        print(invert(t))