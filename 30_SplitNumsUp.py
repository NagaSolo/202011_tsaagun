''' Challenge: Splitting Up Numbers [SplitNumsUp]

Published by: Cool_Kidd

Summary:
    Create a function that takes a number num and returns each place value in the number.

Examples:
    num_split(39) 
        ➞ [30, 9]

    num_split(-434) 
        ➞ [-400, -30, -4]

    num_split(100) 
        ➞ [100, 0, 0]
        
'''

def num_split(num):
    if num < 0:
        num = ['-'+i for i in str(num)[1:]]
        return [int(j+('0'*(len(num)-i-1))) for i, j in enumerate(num)]
    num = [i for i in str(num)]
    return [int(j+('0'*(len(num)-i-1))) for i, j in enumerate(num)]

if __name__ == "__main__":
    tests = [
        num_split(39), #[30, 9]
        num_split(-434), #[-400, -30, -4]
        num_split(100), #[100, 0, 0]
    ]

    for t in tests:
        print(t)