''' Challenge: Calculate the Volume of a Pyramid [PyramidVolume]

Published by _3G_

Summary:
    Create a function that takes the 
    length, width, height (in meters) and 
    output unit and returns the volume of a pyramid to three decimal places in the correct unit.

Examples:
    pyramid_volume(4, 6, 20, "centimeters") ➞ "160000000.000 cubic centimeters"

    pyramid_volume(1843, 1823, 923, "kilometers") ➞ "1.034 cubic kilometers"

    pyramid_volume(18, 412, 93, "millimeters") ➞ "229896000000000.000 cubic millimeters"

Notes:
    The units used are limited to: millimeters, centimeters, meters and kilometers.
    Ensure you return the answer and add the correct unit in the format cubic <unit>.

'''

def pyramid_volume(length, width, height, unit):
    formula = (length * width * height) / 3
    if unit is 'centimeters':
        return "{:.3f} cubic {}".format(formula * 10 ** 6, unit)
    elif unit is 'millimeters':
        return "{:.3f} cubic {}".format(formula * 10 ** 9, unit)
    elif unit is 'kilometers':
        return "{:.3f} cubic {}".format(formula / 10 ** 9, unit)
    return "{:.3f} cubic {}".format(formula, unit)

def pyramid_volume_dict(length, width, height, unit):
    formula = (length * width * height) / 3
    the_unit_formula = {'meters' : formula, 'centimeters' : formula * 10 ** 6, 'millimeters' : formula * 10 ** 9, 'kilometers' : formula / 10 ** 9}
    return "{:.3f} cubic {}".format(the_unit_formula.get(unit), unit)

if __name__ == "__main__":
    tests = [
        [10, 14, 6, "meters"], # "280.000 cubic meters"
        [8, 12, 2, "centimeters"], # "64000000.000 cubic centimeters"
        [92, 1245, 1923, "kilometers"], # "0.073 cubic kilometers"
        [19, 254, 21, "millimeters"], # "33782000000000.000 cubic millimeters"
    ]

    for t in tests:
        print(pyramid_volume(t[0],t[1], t[2], t[3]))
        print(pyramid_volume_dict(t[0],t[1], t[2], t[3]))