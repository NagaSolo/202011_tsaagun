''' Challenge: Super Strict Grading [SuperStrictGrading]

Summary:
    Given a dictionary of student names and a list of their test scores over the semester, 
    return a list of all the students who passed the course (in alphabetical order). 
    
    However, 
    there is one more thing to mention: the pass mark is 100% in everything!

Examples:
    who_passed({
        "John" : ["5/5", "50/50", "10/10", "10/10"],
        "Sarah" : ["4/8", "50/57", "7/10", "10/18"],
        "Adam" : ["8/10", "22/25", "3/5", "5/5"],
        "Barry" : ["3/3", "20/20"]
    }) 
        ➞ ["Barry", "John"]

    who_passed({
        "Zara" : ["10/10"],
        "Kris" : ["30/30"],
        "Charlie" : ["100/100"],
        "Alex" : ["1/1"]
    }) 
        ➞ ["Alex", "Charlie", "Kris", "Zara"]

    who_passed({
        "Zach" : ["10/10", "2/4"],
        "Fred" : ["7/9", "2/3"]
    }) 
        ➞ []

Notes:
    All of a student's test scores must be 100% to pass.
    
    Remember to return a list of student names alphabetically.

'''

def who_passed(students):
    d_p = {k:v for k,v in zip(students.keys(), [all([i if eval(i) == 1 else False for i in students[j]]) for j in students.keys()])}
    return sorted(i for i in d_p.keys() if d_p[i] == True)

if __name__ == "__main__":
    tests = [
        who_passed({
            "John" : ["5/5", "50/50", "10/10", "10/10"],
            "Sarah" : ["4/5", "50/50", "10/10", "10/10"],
            "Adam" : ["3/5", "46/50", "9/10", "10/10"],
            "Barry" : ["5/5", "50/50", "10/10", "10/10"]
        }), # ["Barry", "John"]

        who_passed({
            "Zara" : ["10/10"],
            "Kris" : ["10/10"],
            "Charlie" : ["10/10"],
            "Alex" : ["10/10"]
        }), # ["Alex", "Charlie", "Kris", "Zara"]

        who_passed({
            "Zach" : ["10/10", "2/4"],
            "Fred" : ["10/10", "3/4"]
        }), # []

        who_passed({
            "John" : ["5/5", "50/50", "10/10", "10/10"],
            "Sarah" : ["4/8", "50/57", "7/10", "10/18"],
            "Adam" : ["8/10", "22/25", "3/5", "5/5"],
            "Barry" : ["3/3", "20/20", "5/5", "2/2"]
        }), # ["Barry", "John"]
    ]

    for t in tests:
        print(t)