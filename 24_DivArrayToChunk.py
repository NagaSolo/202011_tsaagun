''' Challenge: Divide Array into Chunks [DivArrayToChunk]

Published by: Helen Yu

Summary:
    Write a function that divides a list into chunks of size n, 
    where n is the length of each chunk.

Examples:
    chunkify([2, 3, 4, 5], 2) 
        ➞ [[2, 3], [4, 5]]

    chunkify([2, 3, 4, 5, 6], 2) 
        ➞ [[2, 3], [4, 5], [6]]

    chunkify([2, 3, 4, 5, 6, 7], 3) 
        ➞ [[2, 3, 4], [5, 6, 7]]

    chunkify([2, 3, 4, 5, 6, 7], 1) 
        ➞ [[2], [3], [4], [5], [6], [7]]

    chunkify([2, 3, 4, 5, 6, 7], 7) 
        ➞ [[2, 3, 4, 5, 6, 7]]

Notes:
    It's O.K. if the last chunk is not completely filled (see example #2).
    Integers will always be single-digit.

Example by Donnamae:
    def chunkify(lst, size):
	    return [lst[i:i+size] for i in range(0, len(lst), size)]

'''

def chunkify(lst, size):
    res = []
    while len(lst) > 0:
        res.append(lst[0:size])
        del lst[0:size]
    return res

if __name__ == "__main__":
    tests = [
        chunkify([2, 3, 4, 5], 2), # [[2, 3], [4, 5]]
        chunkify([2, 3, 4, 5, 6], 2), # [[2, 3], [4, 5], [6]]
        chunkify([2, 3, 4, 5, 6, 7], 3), # [[2, 3, 4], [5, 6, 7]]
        chunkify([2, 3, 4, 5, 6, 7], 1), # [[2], [3], [4], [5], [6], [7]]
        chunkify([2, 3, 4, 5, 6, 7], 7), # [[2, 3, 4, 5, 6, 7]]
        chunkify([2, 3, 4, 5], 3), # [[2, 3, 4], [5]]
        chunkify([2, 3, 4, 5, 6, 7, 8], 3), # [[2, 3, 4], [5, 6, 7], [8]]
    ]

    for t in tests:
        print(t)