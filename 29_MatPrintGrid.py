''' Challenge: Print Grid [MatPrintGrid]

Published by: Pulkit Agarwal

Summary:
    Write a method that accepts two integer parameters rows and cols. 
    
    The output is a 2d array of numbers displayed in column-major order, 
    meaning the numbers shown increase sequentially down each column 
    and wrap to the top of the next column to the right once the bottom of the current column is reached.

Examples;
    printGrid(3, 6) ➞ [
        [1, 4, 7, 10, 13, 16],
        [2, 5, 8, 11, 14, 17],
        [3, 6, 9, 12, 15, 18]
    ]

    printGrid(5, 3) ➞ [
        [1, 6, 11],
        [2, 7, 12],
        [3, 8, 13],
        [4, 9, 14],
        [5, 10, 15]
    ]


    printGrid(4, 1) ➞ [
        [1],
        [2],
        [3],
        [4]
    ]

'''

def printgrid(rows, cols):
    return [[i + j for i in range(1, rows * (cols - 1) + 2, rows)] for j in range(0, rows)]

if __name__ == "__main__":
    tests = [
        printgrid(3, 6),  # [[1, 4, 7, 10, 13, 16], [2, 5, 8, 11, 14, 17], [3, 6, 9, 12, 15, 18]]
        printgrid(5, 3),  # [[1, 6, 11], [2, 7, 12], [3, 8, 13], [4, 9, 14], [5, 10, 15]]
        printgrid(4, 1),  # [[1], [2], [3], [4]]
        printgrid(1, 3),  # [[1, 2, 3]]
        printgrid(10, 2), # [[1, 11], [2, 12], [3, 13], [4, 14], [5, 15], [6, 16], [7, 17], [8, 18], [9, 19], [10, 20]]
    ]

    for t in tests:
        print(t)