''' Challenge: Matrix Transpose [MatTranspose]

Published by: Helen Yu

Summary:
    Create a function that transposes a 2D matrix.

Examples:
    transpose_matrix([
        [1, 1, 1],
        [2, 2, 2],
        [3, 3, 3]
    ]) 
        ➞ [
            [1, 2, 3],
            [1, 2, 3],
            [1, 2, 3]
        ]

    transpose_matrix([
        [5, 5],
        [6, 7],
        [9, 1]
    ]) 
        ➞ [
            [5, 6, 9],
            [5, 7, 1]
        ]

'''

def transpose_matrix(lst):
    return [[lst[i][j] for i in range(0, len(lst))] for j in range(0, len(lst[0]))]

if __name__ == "__main__":
    tests = [
        transpose_matrix([
            [1, 1, 1], 
            [2, 2, 2], 
            [3, 3, 3]
        ]),  # [[1, 2, 3], [1, 2, 3], [1, 2, 3]
        transpose_matrix([
            [1, 1, 1], 
            [2, 2, 2]
        ]), # ([[1, 2],[1, 2],[1, 2]]))
        transpose_matrix([
            [1, 2, 3, 4], 
            [5, 6, 7, 8], 
            [9, 10, 11, 12]
	    ]),  # ([[1, 5, 9],[2, 6, 10],[3, 7, 11], [4, 8, 12]])
    ]

    for t in tests:
        print(t)