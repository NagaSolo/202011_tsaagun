''' Challenge: Underscore-Hash Staircase [MatUScoreHashStair]

Published by: Deep Xavier

Summary:
    Create a function that will build a staircase using the underscore _ and hash # symbols. 
    
    A positive value denotes the staircase's upward direction and downwards for a negative value.

Examples:
    staircase(3) 
        ➞ "__#\n_##\n###"
        
        __#
        _##
        ###

    staircase(7) 
        ➞ "______#\n_____##\n____###\n___####\n__#####\n_######\n#######"

            ______#
            _____##
            ____###
            ___####
            __#####
            _######
            #######

    staircase(2) 
        ➞ "_#\n##"
            
            _#
            ##

    staircase(-8) 
        ➞ "########\n_#######\n__######\n___#####\n____####\n_____###\n______##\n_______#"

            ########
            _#######
            __######
            ___#####
            ____####
            _____###
            ______##
            _______#

Notes:
    All inputs are either positive or negative values.
    
    The string to be returned is adjoined with the newline character (\n).
    
    A recursive version of this challenge can be found in here.

'''

def staircase(n):
    if n > 0:
        return '\n'.join(''.join('_' if i+j < n-1 else '#' for i in range(n)) for j in range(n))
    n = n * (-1)
    return '\n'.join(''.join('#' if i+j < n else '_' for i in range(n))[::-1] for j in range(n))

if __name__ == "__main__":
    tests = [
        3, 7, 2, -8, 4, -12, 11, -6
    ]
    for t in tests:
        print(staircase(t))
        # [
        #     "__#\n_##\n###", 
        #     "______#\n_____##\n____###\n___####\n__#####\n_######\n#######",
        #     "_#\n##",
        #     "########\n_#######\n__######\n___#####\n____####\n_____###\n______##\n_______#",
        #     "___#\n__##\n_###\n####",
        #     "############\n_###########\n__##########\n___#########\n____########\n_____#######\n______######\n_______#####\n________####\n_________###\n__________##\n___________#",
        #     "__________#\n_________##\n________###\n_______####\n______#####\n_____######\n____#######\n___########\n__#########\n_##########\n###########",
        #     "######\n_#####\n__####\n___###\n____##\n_____#"
        # ]