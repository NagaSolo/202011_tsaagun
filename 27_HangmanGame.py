''' Challenge: The Hangman Game [HangmanGame]

Published by: MyName

Summary:
    Create a function that, 
    given a phrase and a number of letters guessed, 
    returns a string with hyphens - for every letter of the phrase not guessed, 
    and each letter guessed in place.

Examples:
    hangman("helicopter", ["o", "e", "s"]) 
        ➞ "-e---o--e-"

    hangman("tree", ["r", "t", "e"]) 
        ➞ "tree"

    hangman("Python rules", ["a", "n", "p", "r", "z"]) 
        ➞ "P----n r----"

    hangman("He"s a very naughty boy!", ["e", "a", "y"]) 
        ➞ "-e"- a -e-y -a----y --y!"

Notes:
    The letters are always given in lowercase, 
    but they should be returned in the same case as in the original phrase (see example #3).
    
    All characters other than letters should always be returned (see example #4).

Example by Caleb Miller (one-liner):
    def hangman(s, l):
        return ''.join('-' if x.isalpha() and x.lower() not in l else x for x in s)

'''

def hangman(phrase, lst):
    lst_u = [i.capitalize() for i in lst] + lst
    for c in phrase:
        if c not in lst_u and c.isalpha():
            phrase = phrase.replace(c, '-', 1)
    return phrase

if __name__ == "__main__":
    tests = [
        hangman("Looney Tunes", ["a", "e", "i", "o", "u"]), # "-oo-e- -u-e-"
        hangman("summer", ["f", "l", "i"]), # "------"
        hangman("Connect-4", ["c", "e", "e"]), # "C---ec--4"
    ]
    for t in tests:
        print(t)