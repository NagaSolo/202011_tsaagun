''' Challenge: Sort by Answer [SortByAnswer]

Published by: Joshua Senoron

Summary:
    Given a list of math expressions, 
    create a function which sorts the list by their answer. 
    
    It should be sorted in ascending order.

Examples:
    sort_by_answer(["1 + 1", "1 + 7", "1 + 5", "1 + 4"]) 
        ➞ ["1 + 1", "1 + 4", "1 + 5", "1 + 7"]

    sort_by_answer(["4 - 4", "2 - 2", "5 - 5", "10 - 10"]) 
        ➞ ["4 - 4", "2 - 2", "5 - 5", "10 - 10"]

    sort_by_answer(["2 + 2", "2 - 2", "2 * 1"]) 
        ➞ ["2 - 2", "2 * 1", "2 + 2"]

Notes:
    If multiple expressions have the same answer, 
    put them in the order of which they appear (see example #2).
    
    You won't need to worry about divisions by zero.
    
    The symbol used for multiplication is x instead of *.

Example by zatoichi49 (short one-liner):
    def sort_by_answer(lst):
	    return sorted(lst, key=lambda x: eval(x.replace('x', '*')))

'''

def sort_by_answer(lst):
    lst = (i.replace('x', '*') for i in lst)
    return [exp.replace('*', 'x') for exp in sorted(lst, key=eval)]

if __name__ == "__main__":
    tests = [
        sort_by_answer(["1 + 1", "1 + 7", "1 + 5", "1 + 4"]), # ["1 + 1", "1 + 4", "1 + 5", "1 + 7"]
        sort_by_answer(["2 + 2", "2 - 2", "2 x 2", "2 / 2"]), # ["2 - 2", "2 / 2", "2 + 2", "2 x 2"]
        sort_by_answer(["1 x 1", "3 x 3", "-1 x -1", "-3 x -3"]), # ["1 x 1", "-1 x -1", "3 x 3", "-3 x -3"]
    ]
    for t in tests:
        print(t)