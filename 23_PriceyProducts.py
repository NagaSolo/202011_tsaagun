''' Challenge: Pricey Products [PriceyProducts]

Published by: Alessandro Manicone

Summary:
    You will be given a dictionary with various products and their respective prices. 
    
    Return a list of the products with a minimum price of 500 in descending order.

Examples:
    pricey_prod({"Computer" : 600, "TV" : 800, "Radio" : 50}) 
        ➞ ["TV", "Computer"]

    pricey_prod({"Bike1" : 510, "Bike2" : 401, "Bike3" : 501}) 
        ➞ ["Bike1", "Bike3"]

    pricey_prod({"Loafers" : 50, "Vans" : 10, "Crocs" : 20}) 
        ➞ []

'''

def pricey_prod(d):
    d_s = sorted(d, key=d.get, reverse=True)
    return [i for i in d_s if d[i] > 499]

if __name__ == "__main__":
    tests = [
        pricey_prod({'Computer' : 600, 'TV' : 800, 'Radio' : 100}), # ['TV','Computer']
        pricey_prod({'Bike1' : 510, 'Bike2' : 401, 'Bike3' : 501}), # ['Bike1', 'Bike3']
        pricey_prod({'Calvin Klein' : 500, 'Armani' : 5000, 'Dolce & Gabbana' : 2000}), # ['Armani', 'Dolce & Gabbana', 'Calvin Klein']
        pricey_prod({'Loafers' : 50, 'Vans' : 10, 'Crocs' : 20}), # []
        pricey_prod({'Dell' : 400, 'HP' : 300, 'Apple' : 1200}), # ['Apple']
    ]

    for t in tests:
        print(t)