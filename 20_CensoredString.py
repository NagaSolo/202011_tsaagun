''' Challenge: C*ns*r*d Str*ngs

Published by: Matt

Summary:
    Someone has attempted to censor my strings by replacing every vowel with a *, l*k* th*s. 
    
    Luckily, 
    I've been able to find the vowels that were removed.

    Given a censored string and a string of the censored vowels, 
    return the original uncensored string.

Example:
    uncensor("Wh*r* d*d my v*w*ls g*?", "eeioeo") 
        ➞ "Where did my vowels go?"

    uncensor("abcd", "") 
        ➞ "abcd"

    uncensor("*PP*RC*S*", "UEAE") 
        ➞ "UPPERCASE"

Notes:
    The vowels are given in the correct order.
    The number of vowels will match the number of * characters in the censored string.

Example by darkploxyo using keyword args:
    def uncensor(txt, vowels):
        txt = txt.replace('*', '{}')
        return txt.format(*vowels)

Example by donnamae, pop():
    def uncensor(txt, vowels):
        v=list(vowels)
        return ''.join(v.pop(0) if i=="*" else i for i in txt)
'''

def uncensor(txt, vowels):
    i = 0
    for c in txt:
        if c == '*':
            # replace(old, new, count), set count to 1 or else all will be replaced simultaneously
            txt = txt.replace(c, vowels[i], 1) 
            i += 1
    return txt

if __name__ == "__main__":
    tests = [
        uncensor('Wh*r* d*d my v*w*ls g*?', 'eeioeo'), # 'Where did my vowels go?'
        uncensor('abcd', ''), # 'abcd', # 'Works with no vowels.'
        uncensor('*PP*RC*S*', 'UEAE'), # 'UPPERCASE', 'Works with uppercase'
        uncensor('Ch**s*, Gr*mm*t -- ch**s*', 'eeeoieee'), # 'Cheese, Grommit -- cheese', 'Works with * at the end'
        uncensor('*l*ph*nt', 'Eea'), # 'Elephant', # 'Works with * at the start'
    ]

    for t in tests:
        print(t)