### Overview
- repo: gitlab.com/NagaSolo/202011_tsaagun

### Summary

### Backend

Various challenges for tsaagun `EDABIT`

- 01st November - `Edabit` - ClassSportsPlayer - `Medium`
    - 5mins - understanding questions
    - 5mins - write, testing, coding craft
    - class, constructor, .format()

- 02nd November - `Edabit` - NotCAPSLOCKDay - `Medium`
    - 5mins - understanding questions
    - 5mins - write, testing, coding craft
    - string, slicing, .capitalize(), .isupper()

- 03rd November - `Edabit` - ClassFullnameEmail - `Medium`
    - 20mins - understanding questions
    - 15mins - write, testing, coding craft
    - string, class, lower()

- 04th November - `Edabit` - NewWordBuilder - `Medium`
    - 10mins - understanding questions
    - 5mins - write, testing, coding craft
    - list comprehension, slicing, .join()

- 05th November - `Edabit` - LengthWithNoLen - `Medium`
    - 3mins - understanding questions
    - 6mins - write, testing, coding craft
    - genexpr, logic, sum()

- 06th November - `Edabit` - IsItGone - `Medium`
    - 3mins - understanding questions
    - 6mins - write, testing, coding craft
    - dict(), keys(), capitalize(), string()

- 07th November - `Edabit` - InvertKeyValue - `Medium`
    - 1mins - understanding questions
    - 3mins - write, testing, coding craft
    - dict(), keys(), values(), dict comprehension, zip()

- 08th November - `Edabit` - PyramidVolume - `Hard`
    - 5mins - understanding questions
    - 25mins - write 2 methods of solution, testing, coding craft
    - interview, .format(), conditionals, dict(), decimal formatting

- 09th November - `Edabit` - GenerateCharPermutations - `Hard`
    - 12mins - understanding questions, understanding permutations itertools
    - 8mins - write, testing, coding craft
    - interview, itertools.permutations(), .join(), sorted(), list comprehension

- 10th November - `Edabit` - ReversibleRange - `Medium`
    - 12mins - understanding questions, understanding step
    - 12mins - write, testing, coding craft
    - interview, step, scope, range(), list comprehension

- 11th November - `Edabit` - Pluralize! - `Hard`
    - 3mins - understanding questions, understanding set()
    - 5mins - write, testing, coding craft
    - set comprehension, strings, set(), arrays, conditionals, list.count()

- 12th November - `Edabit` - Leaderboards - `Hard`
    - 8mins - understanding questions, recall reversing, and tuple()
    - 12mins - write, testing, coding craft
    - sorted, slicing, tuple(), list comprehension, dict access

- 13th November - `Edabit` - UniqueGenre - `Hard`
    - 10mins - understanding questions, recall set(), flattening list
    - 8mins - write, testing, coding craft
    - set comprehension, sum()

- 14th November - `Edabit` - CellDevKboardStr - `Hard`
    - 20mins - understanding questions, strategizing
    - 10mins - write, testing, coding craft
    - dict(), ''.join(), str(), lower()

- 15th November - `Edabit` - PeopleSort - `Very Hard`
    - 15mins - understanding questions, strategizing
    - 10mins - write, testing, coding craft
    - tuple(), class, getattr(), zip(), sorted(), list comprehension

- 16th November - `Edabit` - DecRangeFunc - `Very Hard`
    - 25mins - understanding questions, strategizing, revision on tuple, *args
    - 10mins - write, testing, coding craft
    - tuple(), *args, list comprehension, range(), conditionals

- 17th November - `Edabit` - SmallestMissingPositive - `Hard`
    - 25mins - understanding questions, strategizing, revision on range() set()
    - 25mins - write, testing, coding craft, tricky tests
    - comprehension, range(), conditionals, set(), max(), min()

- 18th November - `Edabit` - AdvSrtLst - `Very Hard`
    - 45mins - understanding questions, strategizing, revision on various topics
    - 45mins - write, testing, coding craft, different python version,
    - `DONE` -> on 28th November, 25mins, implementing set() list.count() as range, bool logic at conditionals
    - comprehension, set(), range(), dict(), one-liner

- 18th November - `Edabit` - AnonName - `Hard`
    - 5mins - understanding questions, strategizing
    - 15mins - write, testing, coding craft, tricky slicing
    - comprehension, slicing, string, tuple, zip()

- 19th November - `Edabit` - StatisticsMode - `Hard`
    - 3mins - understanding questions, strategizing
    - 4mins - write, testing, coding craft, sorting set
    - comprehension, sorted(), set(), statistics, max()

- 20th November - `Edabit` - CensoredString - `Hard`
    - 21mins - understanding questions, strategizing, understanding replace(old, new, count)
    - 9mins - write, testing, coding craft, using count variable
    - comprehension, replace(), for loops, increment cache

- 20th November - `Edabit` - GCDofList - `Hard`
    - 25mins - understanding questions, strategizing, implementing and understanding set.intersection()
    - 15mins - write, testing, coding craft, testing *args implementation
    - comprehension, range(), min(), max(), set.intersection(), *args

- 20th November - `Edabit` - IntersectingIntervals - `Medium`
    - 3mins - understanding questions, strategizing best approach
    - 8mins - write, testing, coding craft
    - generator, sum(), range(), one-liner

- 21st November - `Edabit` - BriscolaPart1 - `Hard`
    - 10mins - understanding questions, strategizing, implementing and understanding if elif else one-liner
    - 15mins - write, testing, coding craft, testing if elif else one-liner
    - comprehension, sum(), dict(), if else if one-liner, dict.keys(), comparisons

- 22nd November - `Edabit` - ToiletPaperEstimator - `Hard`
    - 5mins - understanding questions, strategizing
    - 3mins - write, testing, coding craft
    - comprehension, math, formatting, if else one-liner, dict.get()

- 22nd November - `Edabit` - IntegerComparator - `Hard`
    - 20mins - understanding questions, strategizing, re.sub understanding, swap in enumerate
    - 10mins - write, testing, coding craft, testing various approach
    - comprehension, math, enumerate(), isdigit(), and, eval(), ''.join()

- 22nd November - `Edabit` - DigVowelBan - `Hard`
    - 5mins - understanding questions, strategizing
    - 8mins - write, testing, coding craft, write dict for int to vocabulary
    - ''.join(), int(), dict(), zip(), range(), len(), str(), conditionals

- 23rd November - `Edabit` - SuperStrictGrading - `Hard`
    - 5mins - understanding questions, strategizing
    - 8mins - write, testing, coding craft, write dict
    - dict(), zip(), all(), eval(), comprehension

- 23rd November - `Edabit` - PriceyProducts - `Hard`
    - 5mins - understanding questions, strategizing
    - 10mins - write, testing, coding craft, implementing sorted() key param, reverse param
    - dict(), sorted(), dict slicing, comprehension

- 24th November - `Edabit` - DivArrayToChunk - `Hard`
    - 15mins - understanding questions, strategizing, first time using del
    - 10mins - write, testing, coding craft, implementing del
    - del, while loops, cache, list slicing

- 24th November - `Edabit` - BridgeShuffle - `Hard`
    - 15mins - understanding questions, strategizing, research comprehension on interleaving lists
    - 10mins - write, testing, coding craft, stuck at tricky testcase at first, but got over it
    - cache, len, min(), max(), zip(), comprehension, slicing(list), conditionals(one-liner)

- 24th November - `Edabit` - BestTstAvgStud - `Hard`
    - 5mins - understanding questions, strategizing
    - 10mins - write, testing, coding craft, troubleshooting sum comprehension
    - sum(), min(), max(), comprehension, dict.keys(), dict.values(), ''.join()

- 25th November - `Edabit` - EmployeeParsing - `Hard`
    - 10mins - understanding questions, strategizing, understanding class method usage
    - 30mins - write, testing, coding craft, implementing class method
    - class, @classmethod, string.split()

- 25th November - `Edabit` - UndulatingNumbers - `Hard`
    - 10mins - understanding questions, strategizing
    - 10mins - write, testing, coding craft, testing all()
    - all(), len(), conditionals(one-liner), set(), str(), slicing(list)

- 25th November - `Edabit` - EverySome - `Hard`
    - 3mins - understanding questions, strategizing
    - 7mins - write, testing, coding craft, testing all(), testing any()
    - all(), any(), conditionals(one-liner), eval(), str(), comprehension(generator)

- 26th November - `Edabit` - PicFrame - `Hard`
    - 15mins - understanding questions, strategizing, pattern printing with nested comprehension
    - 25mins - write, testing, coding craft, testing pattern printing using nested comprehension
    - logic, conditionals, range(), pattern printing, comprehension(generator), ''.join(), comprehension(nested)

- 26th November - `Edabit` - CheckerBoardGen - `Hard`
    - 5mins - understanding questions, strategizing, pattern printing with nested comprehension
    - 5mins - write, testing, coding craft, testing pattern printing using nested comprehension
    - logic, conditionals, range(), pattern printing, comprehension(generator), ''.join(), comprehension(nested)

- 26th November - `Edabit` - SimpleSentenceFmt - `Hard`
    - 7mins - understanding questions, strategizing, figuring logic for formatting
    - 8mins - write, testing, coding craft, testing formatting
    - logic, conditionals, ''.join(), comprehension(generator), string(+ operator), capitalize(), slicing(string)

- 27th November - `Edabit` - IceCreamSandwich - `Hard`
    - 7mins - understanding questions, strategizing
    - 8mins - write, testing, coding craft, testing reversing by slicing
    - bool, conditionals, conditionals(one-liner), slicing(string), len(), set()

- 27th November - `Edabit` - HangmanGame - `Hard`
    - 5mins - understanding questions, strategizing
    - 15mins - write, testing, coding craft, wasted time on comprehension implementation
    - str.replace(old, new, count), str.capitalize(), for loops, bool, isalpha(), comprehension

- 27th November - `Edabit` - SortByAnswer - `Hard`
    - 5mins - understanding questions, strategizing
    - 15mins - write, testing, coding craft, implementing sorted() with key=eval, stuck at replacing back 'x'
    - str.replace(old, new), sorted([], key=function), comprehension(generator), comprehension(list), str.replace(), eval()

- 28th November - `Edabit` - WordsNameAnagram - `Hard`
    - 5mins - understanding questions, strategizing
    - 3mins - write, testing, coding craft, implementing comprehension
    - str.lower(), comprehension(gen), logic bool, sorted(), ''.join()

- 28th November - `Edabit` - MatTranspose - `Hard`
    - 5mins - understanding questions, strategizing
    - 8mins - write, testing, coding craft, implementing comprehension for matrix
    - math(matrix), comprehension(list), one-liner, range(), len()

- 29th November - `Edabit` - MatCanSeeStage - ` Very Hard`
    - 5mins - understanding questions, strategizing
    - 5mins - write, testing, coding craft, implementing comprehension for matrix
    - math(matrix), comprehension(list), all(), sorted(), set(), range(), len()

- 29th November - `Edabit` - MatPrintGrid - `Hard`
    - 5mins - understanding questions, strategizing
    - 8mins - write, testing, coding craft, implementing comprehension for matrix
    - math(matrix), comprehension(nested list), one-liner, range()

- 29th November - `Edabit` - IncrWordsWeight - `Hard`
    - 5mins - understanding questions, strategizing
    - 8mins - write, testing, coding craft, implementing comprehension, patching last tricky test
    - comprehension(list), bool logic, one-liner, string[isalpha(), ord(), split()], sum(), sorted(), set()

- 30th November - `Edabit` - MatUScoreHashStair - ` Very Hard`
    - 10mins - understanding questions, strategizing, stuck at test involving negative number
    - 25mins - write, testing, coding craft, implementing comprehension for matrix
    - math(matrix), pattern-printing, ''.join(), one-liner, conditionals, comprehension(gen), range()

- 30th November - `Edabit` - SplitNumsUp - ` Very Hard`
    - 10mins - understanding questions, strategizing, strategy at test involving negative number
    - 10mins - write, testing, coding craft, implementing comprehension
    - enumerate(), ''.join(), conditionals, comprehension(gen, list), slicing(string), range()