''' Challenge: Generate All String Character Permutations [GenerateCharPermutations]

Published by: Sinomede

Summary:
    Create a function to generate all string character permutations.

Examples:
    permutations("AB") ➞ "AB BA"

    permutations("CD") ➞ "CD DC"

    permutations("EF") ➞ "EF FE"

    permutations("NOT") ➞ "NOT NTO ONT OTN TNO TON"

    permutations("RAM") ➞ "AMR ARM MAR MRA RAM RMA"

    permutations("YAW") ➞ "AWY AYW WAY WYA YAW YWA"

Notes:
    You must sort your results in alphabetical order before returning them.

'''

def permutations(s):
    from itertools import permutations
    return ' '.join(sorted(''.join(c for c in el) for el in list(permutations(s))))

if __name__ == "__main__":
    tests = [
        "AB", # "AB BA"
        "CD", # "CD DC"
        "EF", # "EF FE"
        "NOT", #"NOT NTO ONT OTN TNO TON"
    ]

    for t in tests:
        print(permutations(t))