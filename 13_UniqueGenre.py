''' Challenge: How Many Unique Styles? [UniqueGenre]

Published by: persolut

Summary:
    There are many different styles of music and many albums exhibit multiple styles. 
    Create a function that takes a list of musical styles from albums and returns how many styles are unique.

Examples:
    unique_styles([
    "Dub,Dancehall",
    "Industrial,Heavy Metal",
    "Techno,Dubstep",
    "Synth-pop,Euro-Disco",
    "Industrial,Techno,Minimal"
    ]) ➞ 9

    unique_styles([
    "Soul",
    "House,Folk",
    "Trance,Downtempo,Big Beat,House",
    "Deep House",
    "Soul"
    ]) ➞ 7

'''

def unique_styles(albums):
    return sum(1 for el in set([c for el in [s.split(',') for s in albums] for c in el]))

if __name__ == "__main__":
    tests = [
        ["Dub,Dancehall", "Industrial,Heavy Metal", "Techno,Dubstep", "Synth-pop,Euro-Disco", "Industrial,Techno,Minimal"],
        ["Soul", "House,Folk", "Trance,Downtempo,Big Beat,House", "Deep House", "Soul"]
    ]

    for t in tests:
        print(unique_styles(t))