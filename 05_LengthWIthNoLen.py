''' Challenge: Length of Number [LengthWithNoLen]

Published by: Cool Kid

Summary:
    Create a function that takes a number num and returns its length.

Examples:
    number_length(10) ➞ 2

    number_length(5000) ➞ 4

    number_length(0) ➞ 1

Notes:
    DO NOT USE LEN() FOR THIS CHALLENGE
    
'''

def number_length(num):
    return sum(1 for i in str(num))

if __name__ == "__main__":
    tests = [
        10, 5000, 0, 4039182, 9999999999999999, 
    ]
    # return 2, 4, 1, 7, 16

    for t in tests:
        print(number_length(t))