''' Challenge: Modify Words [ModWords]

Published by: werdna

Summary:
    Create a function that takes a list of any length. 
    
    Modify each element (capitalize, reverse, hyphenate).

Examples:
    edit_words(["new york city"]) 
        ➞ ["YTIC KR-OY WEN"]

    edit_words(["null", "undefined"]) 
        ➞ ["LL-UN", "DENIF-EDNU"]

    edit_words(["hello", "", "world"]) 
        ➞ ["OLL-EH", "-", "DLR-OW"]

    edit_words([""]) 
        ➞ ["-"]

Notes:
    Input list values can be any type.

'''

def edit_words(lst):
    return [(w[0:len(w)//2] + '-' + w[len(w)//2:])[::-1].upper() for w in lst]

if __name__ == "__main__":
    tests = [
        edit_words(["javascript"]), # ["TPIRC-SAVAJ"]
        edit_words(["hello", "", "world"]), # ["OLL-EH", "-", "DLR-OW"]
        edit_words(["null", "undefined"]), # ["LL-UN", "DENIF-EDNU"]
    ]

    for t in tests:
        print(t)