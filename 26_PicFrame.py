''' Challenge: Creating a Picture Frame [PicFrame]

Published by: Joshua Senoron

Summary:
    Create a function that takes the width, 
    height and character and returns a picture frame as a 2D list.

Examples:
    get_frame(4, 5, "#") 
        ➞ [
            ["####"],
            ["#  #"],
            ["#  #"],
            ["#  #"],
            ["####"]
        ]
        # Frame is 4 characters wide and 5 characters tall.


    get_frame(10, 3, "*") 
        ➞ [
            ["**********"],
            ["*        *"],
            ["**********"]
        ]
        # Frame is 10 characters and wide and 3 characters tall.


    get_frame(2, 5, "0") 
        ➞ "invalid"
        # Frame"s width is not more than 2.

Notes:
    Remember the gap.
    
    Return "invalid" if width or height is 2 or less (can't put anything inside).

'''

def get_frame(w, h, ch):
    if (w or h) < 3:
        return 'invalid'
    return [[''.join(ch for i in range(w))] if j in [0, h-1] else [''.join(ch if i in [0, w-1] else ' ' for i in range(w))] for j in range(h)]

if __name__ == "__main__":
    tests = [
        get_frame(3, 3, "0"), # [["000"],["0 0"],["000"]]
        get_frame(4, 5, "#"), # [["####"],["#  #"],["#  #"],["#  #"],["####"]]
        get_frame(10, 3, "*"), # [["**********"],["*        *"],["**********"]]
        get_frame(2, 5, "0"), # "invalid"
        get_frame(1, 6, "["), # "invalid"
        get_frame(5, 4, "z"), # [["zzzzz"], ["z   z"], ["z   z"], ["zzzzz"]]
    ]

    for t in tests:
        print(t)