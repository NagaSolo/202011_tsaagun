''' Challenge: Pluralize!

Published by: Joshua Senoron

Summary:
    Given a list of words in the singular form, 
    return a set of those words in the plural form if they appear more than once in the list.

Examples:
    pluralize(["cow", "pig", "cow", "cow"]) ➞ { "cows", "pig" }

    pluralize(["table", "table", "table"]) ➞ { "tables" }

    pluralize(["chair", "pencil", "arm"]) ➞ { "chair", "pencil", "arm" }

Notes:
    This is an oversimplification of the English language so no edge cases will appear.
    Only focus on whether or not to add an s to the ends of words.
    All tests will be valid.

'''

def pluralize(lst):
    return set(el + 's' if lst.count(el) > 1 else el for el in lst)

if __name__ == "__main__":
    tests = [
        ["cow", "pig", "cow", "cow"], # {"cows", "pig"}
        ["table", "table", "table"], # {"tables"}
        ["chair", "pencil", "arm"], # {"chair", "pencil", "arm"}
        ["list"], # {"list"}
    ]

    for t in tests:
        print(pluralize(t))