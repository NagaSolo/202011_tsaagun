''' Challenge: Basic Statistics: Mode [StatisticsMode]

Published by: zatoichi49

Summary:
    The mode of a group of numbers is the value (or values)
    that occur most often (values have to occur more than once). 
    
    Given a sorted list of numbers, 
    return a list of all modes in ascending order.

Examples:
    mode([4, 5, 6, 6, 6, 7, 7, 9, 10]) 
        ➞ [6] 

    mode([4, 5, 5, 6, 7, 8, 8, 9, 9]) 
        ➞ [5, 8, 9]

    mode([1, 2, 2, 3, 6, 6, 7, 9]) 
        ➞ [2, 6] 

Notes:
    In this challenge, 
    all group of numbers will have at least one mode.

'''

def mode(nums):
    mode = max([nums.count(n) for n in set(nums)])
    return sorted([n for n in set(nums) if nums.count(n) == mode])

if __name__ == "__main__":
    tests = [
        mode([1, 2, 3, 3, 6, 7, 8, 9]), # [3]
        mode([2, 3, 3, 4, 4, 6, 7, 8]), # [3, 4]
        mode([1, 6, 6, 7, 7, 8, 9]), # [6, 7]
        mode([4, 4, 4, 6, 8, 9, 10, 10]), # [4]
        mode([1, 4, 6, 7, 9, 9]), # [9]
        mode([2, 2, 2, 3, 7, 8, 9, 9]), # [2]
        mode([2, 4, 5, 5, 7, 8, 10, 10]), # [5, 10]
        mode([1, 1, 5, 6, 6, 10, 10]), # [1, 6, 10]
    ]
    for t in tests:
        print(t)