''' Challenge: Validate the Relationships Between Integers in a String [IntegerComparator]

Published by: Matt

Summary:
    You will be given a string consisting of a list of integers 
    and their relationships to their neighboring integers. 
    
    For instance:

        "-15<-10<=0=0<5"
    
    Test to see that all the relationships between the integers in the string are true. 
    
    If they are, 
    return True. 
    
    If they are not, 
    return False.

Examples:
    validate_relationships("5>-1<0=0<-5>5=5") 
        ➞ False
        # This is False because 0 is not less than -5.

    validate_relationships("-15<-10<=0=0<5") 
        ➞ True

    validate_relationships("0=807<1000<=1000>9990<-3605<=20") 
        ➞ False
        # This is False because 0 is not equal to 807.

Notes:
    This is a modifcation of Helen Yu's "Correct Signs" challenge.
    
    As the examples above show, 
    there could be negative numbers in the string.
    
    The numbers will only be separated by: =, <, >, >=, <=
    
    Tests will not contain any spaces.

Example by zatoichi49:
    def validate_relationships(txt):
        return eval(txt.replace('=', '==').replace('<==', '<=').replace('>==', '>='))

'''

def validate_relationships(txt):
    lst = [c for c in txt]
    for i,j in enumerate(lst):
        if j == '=' and lst[i-1].isdigit():
            lst[i] = '=='
    return eval(''.join(c for c in lst))

if __name__ == "__main__":
    tests = [
        validate_relationships("5>-1<0=0<-5>5=5"), # False
        validate_relationships("-15<-10<=0=0<5"), # True
        validate_relationships("0=807<1000<=1000>9990<-3605<=20"), # False
        validate_relationships("3<=3<11>-109"), # True
        validate_relationships("44>-33>=1>-13"), # False
        validate_relationships("10>2=22>9>3"), # False
        validate_relationships("44>0<13>=-2<-1=-1"), # True
        validate_relationships("3>=-1"), # True
    ]

    for t in tests:
        print(t)