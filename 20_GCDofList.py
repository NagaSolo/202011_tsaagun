''' Challenge: Common Divisor of List [GCDofList]

Published by: lllll

Summary:
    Write a function that returns the greatest common divisor of all list elements. 
    
    If the greatest common divisor is 1, 
    return 1.

Examples:
    GCD([10, 20, 40]) 
        ➞ 10

    GCD([1, 2, 3, 100]) 
        ➞ 1

    GCD([1024, 192, 2048, 512]) 
        ➞ 64

Notes:
    List elements are always greater than 0.
    There is a minimum of two list elements given.
    
'''

def GCD(lst):
    # calculate common divisor of smallest number
    min_cd = [i for i in range(1, min(lst) + 1)if min(lst) % i == 0]

    # calculate all common divisor of other numbers based on smallest number divisors, put it into set
    cd = [set([j for j in min_cd if n % j == 0]) for n in lst]

    # return biggest number from set intersection of all numbers in list common divisors
    return max(set.intersection(*cd))

if __name__ == "__main__":
    tests = [
        GCD([10, 20, 40]), # 10
        GCD([7, 11]), # 1
        GCD([156, 84, 60, 1188, 252]), # 12
        GCD([1, 2, 3, 4, 5]), # 1
        GCD([777, 77, 7, 14]), # 7
        GCD([20, 20, 40, 100]), # 20
        GCD([731, 43, 473, 2623]), # 43
    ]

    for t in tests:
        print(t)